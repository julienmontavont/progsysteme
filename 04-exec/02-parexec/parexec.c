#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <sys/wait.h>
#include <unistd.h>

#define CHK(op)                                                                \
    do {                                                                       \
        if ((op) == -1) {                                                      \
            perror (#op);                                                      \
            exit (1);                                                          \
        }                                                                      \
    } while (0)

#define SIZE 2

noreturn void raler (int syserr, const char *msg, ...)
{
    va_list ap;

    va_start (ap, msg);
    vfprintf (stderr, msg, ap);
    fprintf (stderr, "\n");
    va_end (ap);

    if (syserr == 1)
        perror ("");

    exit (EXIT_FAILURE);
}

void concatener (char *result, int size, char *format, ...)
{
    va_list ap;
    va_start (ap, format);

    int n = vsnprintf (result, size, format, ap);
    if (n < 0 || n >= size)
        raler (0, "snprintf %s", result);

    va_end (ap);

    return;
}

int main (int argc, char *argv [])
{
}
